# Example start script for Atlassian Stash

To install the script run

	cd /path
	cp stash /etc/init.d/
	update-rc.d stash defaults
	
To remove the script run

	rm /etc/init.d/stash
	update-rc.d stash remove
	
Usage: 

	/etc/init.d/stash {start|stop|status|restart|force-reload}
	

